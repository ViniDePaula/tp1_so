#include <iostream>
#include <thread>
#include <time.h>
#include <mutex>
#include <bits/stdc++.h> 
#include <vector>
using namespace std;

std::mutex vectLock; //proteger os dados compartilhados por multiplos threads
std::vector<unsigned int> primeVect;    //vetor que vai guardar os primos
 
void FindPrimes(unsigned int start, 
        unsigned int end){
    int flag;

    while (start < end)
    {
        flag = 0;
        for(int i = 2; i <= start/2; ++i)
        {
            if(start % i == 0)             //para cada valor no intervalo procura multiplos ate valor/2
            {
                flag = 1;
                break;
            }
        }
        if (flag == 0){
            vectLock.lock();                //permite o armazenamento do numero primo no vetor, compartilhando entre os threads
            primeVect.push_back(start);
            vectLock.unlock();
        }
        ++start;
    }    
}
 
void FindPrimesWithThreads(unsigned int start, 
        unsigned int end,
        unsigned int numThreads){
    
    std::vector<std::thread> threadVect;
    
    // Divide o calculo para cada thread 
    // opera em diferentes intervalos

    unsigned int threadSpread = end / numThreads;
    unsigned int newEnd = start + threadSpread - 1;
    
    // Criando lista de primos pra cada thread
    for(unsigned int x = 0; x < numThreads; x++){
        threadVect.emplace_back(FindPrimes,
                start, newEnd);
        
        start += threadSpread;
        newEnd += threadSpread;
    }
    
    for(auto& t : threadVect){
        t.join();
    }
 
}


int main()
{
    int x;
    int li, ls;
    int nT;

    cout << "1- Com threads // 2- Sem threads" << endl;
    cin >> x;

    cout << "Limite inferior, limite superior" << endl;
    cin >> li;
    cin >> ls;

    if(x == 1){
        cout << "Numero de threads" << endl;
        cin >> nT;

        // Tempo antes de começar
        int startTime = clock();
        
        FindPrimesWithThreads(li, ls, nT);
        
        // tempo após o cálculo
        int endTime = clock();
        
        sort(primeVect.begin(), primeVect.end());

        for(auto i: primeVect)
            std::cout << i << "\n";
    

        std::cout << "Execution Time : " << 
                (endTime - startTime)/double(CLOCKS_PER_SEC) 
                << std::endl;
    }

    if(x == 2){
        int startTime = clock();
        
        FindPrimes(li, ls);
        
        int endTime = clock();

        sort(primeVect.begin(), primeVect.end());

        for(auto i: primeVect)
            std::cout << i << "\n";
    
        std::cout << "Execution Time : " << 
                (endTime - startTime)/double(CLOCKS_PER_SEC) 
                << std::endl;
    }

    return 0;
}